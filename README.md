A little micro project to practice HTML, CSS & JS.
It's a random quote generator that lets you guess if the
quote is attributed to George Carlin or Frank Zappa.
Originally I was going to use a 3rd party API quote
generator, but the ones I found were pretty lame, so 
instead I hard coded quotes that would make my grandpa
proud. There is a direct-tweet button to tweet the 
current quote. The animated background is a lottie file.

You can see all the action by simply hovering over 
[this link.](https://gyazo.com/8c40da00362f0fa9497a416c37b1383d)
